-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2018 a las 14:01:21
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `contenido` longtext COLLATE utf8_spanish_ci NOT NULL,
  `autor` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`id`, `titulo`, `contenido`, `autor`, `fecha`) VALUES
(1, 'Qué ver en el Marruecos central (y cómo): el Atlas', 'Lo bueno y lo malo de Marruecos es que está lo suficientemente cerca de Europa y hay compañías aéreas lo suficientemente baratas como para que vayamos todos. Quicir: está muy bien que podamos ir todos, peeeeero, hay veces que la cosa parece la gran vía madrileña: hasta las cartolas de zoombies buscando en qué gastarse el dinero o dónde hacerse un selfie.\r\nSin rencor.\r\nEl asunto viene al caso. Que yo despotricar por deporte… sólo de cañas y con la cuadrilla. No, en serio. Es un problema esto de los zoombies. Se monta en torno a ellos -bueno, quién sabe si fue primero el huevo o la gallina- un negocio que me da mucho repelús, pero que además impide el conocimiento real de los lugares que se visitan y de sus gentes. No hay intercambio cultural de ningún tipo, sólo comercial. Una se convierte por tanto en una especie de cajero automático con patas con la que toda conversación que se establece está destinada única y exclusivamente a sacarle la pasta. Pero ¿qué relación intentarías establecer con una persona que sabes 14 veces más rica que tú, que no tiene interés en conocerte de verdad, y que pasa por allí sin emocionarse realmente con nada porque a nada le presta atención? ¿Y qué atención le prestarías tú a nada si tienes 10 minutos para ver esto. Foto. 15 minutos para ver lo otro. Foto. Foto. Foto. Ponte así. Mira guapa, más barato que el mercadona? ¡Aaaaa! ¡Mátame camión!\r\n\r\nEso es lo que ocurre si contratas una excursión en minibús para visitar el Atlas. Puedes hacer la ruta que te voy a detallar en 3 días. Puede que sea incluso más barato. Pero, desde luego, te recomiendo que inviertas 7 u 8. Que respires los vientos de la montaña, te pierdas en los palmerales, entierres los pies en la arena y te refresques con el agua de los ríos. Que entables conversaciones a base de mímica con mujeres que no entienden que quieras caminar y te arrastren a la carretera para pararte un taxi. Que te sienten a un niño en las rodillas de un autobús sin aire acondicionado. Que te guarde la mochila un chaval que alquila flotadores con cabeza de dinosaurio mientras saltas de las piedras bajo las cascadas. Que te salgas, en fin, del camino de baldosas amarillas.\r\nMoraleja: Huye del minibus. Otra alternativa al autobús público puede ser alquilar un coche y jugarse la vida por las carreteras marroquíes, o contratar un 4×4 con chófer y marcarle tú los tiempos, pero esto es carísimo, al menos conforme a mi criterio, pero soy una ratita, así que lo mismo te compensa.\r\n\r\nY recuerda que estos son los imprescindibles. Si tienes tiempo… hay millones de rincones que visitar en esta zona.', 'Pepito Grillo', '2018-05-07 00:00:00'),
(2, 'Rotterdam y los arquitectos', 'Digamos que no puedo hacer grandes recomendaciones sobre Rotterdam teniendo en cuenta que sólo pasé allí 5 horas, pero creo que puedo decir sin temor a equivocarme que es un destino a tener en cuenta si te interesa la arquitectura. Para empezar os contaré que dam significa presa, y la ciudad nace de un grupo de pescadores que se asientan alrededor del río Rotter allá por 1270. La ciudad crece y crece… hasta mayo de 1940. Entonces llega el ejército nazi, la bombardea y destroza todo lo que pilla, que fue casi todo. Al terminar la guerra los roterdameses deciden darse a la modernidad y apuestan por convertir el lugar en un lienzo perfecto para arquitectos con ganas de experimentar. Tradición que, por lo que parece… se mantiene.\r\n\r\nMe llamaron la atención especialmente dos construcciones: Las casas cubo (Kijk-kubus -a tope con mi holandés-) y el mercado Sumo Markthal. Están una enfrente de la otra y se puede llegar en metro o en tranvía (21 ó 24), parada Blaak, aunque teniendo en cuenta el tamaño de la ciudad y los precios del transporte, recomiendo sin durar otra vía: tus piernas.\r\n\r\nLas Kijk-kubus fueron construidas en 1984. Son obra del arquitecto alemán Piet Blom, que al parecer concibió cada una de las casas como un árbol abstracto y el complejo como un bosque. Si tienes el día inspirado lo verás claro.\r\n\r\nHay 38 apartamentos (sí, hay gente que vive ahí), dos cubos más grandes y otros 14 espacios más pequeños que se usan como tiendas u oficinas. Cada casa cubo tiene tres plantas, y un trastero y un hall en lo que sería el tronco del árbol en cuestión. Blom, -que fue un importante referente de la corriente estructuralista-, construyó el “Blaak forest” por encargo del Ayuntamiento de Rotterdam, que buscaba la manera de comunicar de manera segura el puerto antiguo -que fue, por otro lado, lo único que sobrevivió a la Guerra- con el centro económico de la ciudad.\r\n\r\nEl Markthal es más moderno, pero una también se vuelve loca imaginando la distribución de los apartamentos en su interior. Yo no sé qué muebles pondrá esta gente para aprovechar el espacio, la verdad.\r\n\r\nMoraleja: Aprovecha las escalas. Cada rincón del planeta merece una visita y tiene una lección para ti.', 'Luisito Pitito', '2018-05-16 00:00:00'),
(3, '“Saber de vino es saber si te gusta o no”', '“Saber de vino es saber si te gusta o no”, dijo en la comida. Y así es como José Moro me convenció de que era un tío guay. Esa frase me la ha dicho mi padre millones de veces. El pobre, que no podía vivir con la pena de que a su primogénita querida no le gustara el vino. Y el hombre fue dándome a probar unos y otros, y yo que nada. Hasta que llegámos al gran reserva. Jódete Carpanta, que diría mi madre. Yo no es que sea ignorante, es que soy de morro fino. Lo que pasa es que la cigüeña se lió -porque en aquella época no había smartphones con google maps- y me tiró en una familia humilde. Pero yo tenía que haber nacido rica.\r\n\r\nEl karma me recompensa de vez en cuando, y me da oportunidades como la de visitar las Bodegas Emilio Moro, hacer una cata de vino en barrica y conocer al nieto del fundador, José Moro.\r\n\r\nOs contaré que el taller de cata en barrica es bastante especial. Tomas el vino a medio hacer. Sí, a medio hacer. El mismo vino, reposado en distintas barricas durante el mismo tiempo. Y mola porque así descubres los matices que aparecen con los meses. Y las diferencias que produce elegir una barrica de madera de roble americano con respecto a una barrica de madera de roble francés. No vacilo, se nota un montón.\r\n\r\nY aprendes lo complicadísimo que es hacer un buen vino.\r\n\r\nMe impresionó especialmente el vino Emilio Moro Clon de la Familia. Para empezar por el precio. 200 euracos la botellita de marras. Un placer al alcance de pocos, pero la buena noticia es que está exquisito y que todo lo recaudado con las ventas de esta maravilla se destinan a la Fundación Emilio Moro. Así que es un trago caro pero solidario.\r\nMoraleja: Seguramente debería hablar de algo relacionado con las vides, las uvas o el vino, pero lo más importante que aprendí de la familia Moro aquel día -o en lo que me reafirmé- es que, para ser feliz, una tiene que dedicar su vida a aquello que realmente le apasione.', 'Carlitos Litos', '2018-05-23 00:00:00'),
(4, 'El mirador de la memoria', 'En pleno Valle del Jerte, en la provincia de Cáceres, hay un pueblecito adorable que se llama El Torno. La asociación Jóvenes del Valle del Jerte, Nuestra Memoria eligió esta zona para rendir homenaje a los olvidados de la guerra civil y consiguió una subvención, en el marco de la Ley de Memoria Histórica, para colocar -en un lugar más que privilegiado- una obra del escultor Francisco Cedenilla. Cuatro esculturas a tamaño real dibujan a una familia de represaliados por el franquismo.\r\n\r\nEl Mirador de la Memoria se inauguró en la mañana del 24 enero de 2009. En la tarde del mismo día, un vecino de la zona -un tal Paco, me cuentan- disparó en el hombro izquierdo de las figuras que representan al hijo, al padre y al abuelo. Dejó intacta a la mujer. El escultor no quiso reparar la obra. En aquel momento dijo que le parecía “impresionante” que alguien hubiera querido “volver a fusilarlos”, y consideró que los agujeros de bala le daban aún más sentido a la obra.\r\n\r\nEl tipo al que los vecinos atribuyen los disparos nunca admitió la autoría de los hechos. Defendió entonces -y hasta el día de hoy- que quien disparó fue su hijo, de 14 años. Que le cogió el arma del coche en un descuido, dice la hemeroteca.\r\n\r\nEl crío pagó el pato y acabó en un Centro de Internamiento para menores. Reformatorio para los antiguios. La verdad… probablemente no la conozcamos nunca.\r\n\r\nAhora, algunos lugareños llaman a la escultura “los hombres de Paco”, supongo que porque había mucho Francisco implicado en la película, y porque por aquella época echaban en Telecinco aquella serie del mismo nombre y dudosa calidad.\r\nMoraleja: Hoy no tengo nada divertido para vosotros. Estando allí tuve que esconder las lágrimas tras los cristales de las gafas de sol y no fui capaz siquiera de sacar una foto del impresionante paisaje que se ve desde el Mirador. La moraleja es, supongo, que Cedenilla acertó al negarse a reparar las esculturas. Tal vez así recordemos que España es el segundo país del mundo con mayor número de desaparecidos -sólo por detrás de Camboya- y que hasta que no desenterremos los cuerpos y cerremos las heridas, el odio seguirá teniendo una ventana por la que asomar de vez en cuando.', 'Anónimo', '2018-05-28 00:00:00'),
(5, 'LO QUE PASA EN CUALQUIER TRASPORTE PUBLICO:', '—¡Hola mi gente, una Venezuela educada que me diga un \"Buenos días\"!\r\n—Buenos días.—Aquí la mayoría de los pasajeros esconden sus celulares, relojes y carteras. \r\n—Hoy les traigo estos Bubbaloo que en cualquier tienda cuestan 200 bolivitas fuertes... Pero hoy se los dejare a 50 bolívares.—Aquí es cuando te das cuenta que tu salario no paga ni un chicle.\r\nEl señor pasa por cada uno de los asientos ofreciendo los chicles que tu no sabes si tienen droga o algo a dentro, cuando llega a tu asiento:\r\n—Mami esta barato.\r\n—No gracias—dice tu mamá en un tono muy \"amable\" —Tu no sabes lo que tiene eso.\r\nY en esta parte tu madre te cuenta una historia que le pasó al hijo del vecino del tío de tu primo, y tu que te la crees de rebelde miras hacia la ventana mientras el señor de los chicles se va pobre.', 'Wili Wili', '2018-05-24 00:00:00'),
(6, 'El sexo mueve al mundo', 'Alguna vez te has puesto a pensar de forma objetiva y racional las maneras en las que el sexo hace girar al mundo, y no, no estoy perdiendo la cabeza o sea yo una pervertida, nel, yo soy una persona normal, si se pudiese decir, pero no podemos tapar el sol con un dedo y en esta cultura, en esta sociedad que de alguna manera se encarga de recordarnos que el sexo es malo, también lo promueve como si fuera el motor con el que gira y se impulsa el mundo, que digo, no lo promueve, es el motor con el que gira.\r\n\r\nHace poco me encontré un documental muy interesante sobre el sexo y como ha revolucionado al mundo, si bien para muchos únicamente el sexo se usa para vendernos cosas mercadológicamente hablando, este es un tema mucho más amplio y el sexo pone en acción a todos, no solo hombres sino también mujeres, es el sexo y no el amor lo que hace que el mundo funcione y circule. Aunque claro, seguro por esa declaración mía yo ya entre en la categoría polémica y para algunos de moral baja y laxa, pero ciertamente mi aproximación al sexo es diferente, es natural y la entiendo como algo normal, algo parte de mi vida, la tuya y la de todos.\r\n\r\nTodos, a cualquier edad.\r\n\r\nY eso no está mal, el sexo debería ser un tema natural, como hablar de cualquier otra cosa, claro, con la debida aproximación, una en la que el sexo no es algo morboso, no sea algo ajeno a nosotros, no sea algo malo o amoral; el sexo debería ser parte de nuestra vida, entendiéndolo como una necesidad, luego como un placer y finalmente para entenderlo como una forma de conexión con alguien especial en nuestra vida.\r\n\r\nEl sexo no debería ser malo, no debería causarnos morbo hablar de él, no debería ser algo penoso, no debería ser algo ajeno a nuestra vida, el sexo es parte de nosotros, digo, el sexo ya actualmente mueve al mundo, es usado para vendernos cualquier producto, es más, incluso el sexo es usado para vendernos una idea de lo que deberíamos ser o como deberíamos lucir. El sexo, nos guste o no, mueve al mundo, nos mueve a nosotros, entonces porque verlo de una forma extraña y como si fuera malo, cuando simplemente es parte de nuestras naturaleza, de quienes somos.', 'Julita Pijita', '2018-05-27 00:00:00'),
(7, '¿De dónde surge el término ‘cachondeo’?', 'Se denomina ‘cachondeo’ al jolgorio, hacer burla, la falta de seriedad o a los momentos de desorden y desbarajuste.\r\n\r\nEs un término que puede abarcar desde el acto de estar pasándoselo bien en una fiesta o lugar de ocio (‘Fulanito se ha ido toda la noche de cachondeo’) a la guasa o mofa que se puede hacer hacia otra persona (‘Menganito se está cachondeando de mí’) e incluso se deriva hacia un vocablo con el que se señala un estado de excitación sexual (‘Zutano va cachondo perdido’).\r\n\r\nEl origen etimológico de este término está muy discutido y aunque hay tras de sí toda una provincia que defiende y reclama la autoría de la palabra nada queda en claro, al menos para los expertos.  Voy a intentar daros las diferentes opciones que se barajan sobre el mismo.\r\n\r\nPor una parte nos encontramos con quienes defienden que el origen del término ‘cachondeo’ proviene de Cádiz, más concretamente de Zahara de los Atunes, una localidad bañada por el río Cachón y es precisamente éste el que dicen que dio pie a la palabra. Según cuentan los zahareños, en la rivera de este río era donde se juntaban los pescadores de atún (conocidos como almadraberos) y organizaban sus fiestas y jolgorios tras la jornada de trabajo.\r\n\r\nAlgunos indican que allí era donde solían rondar las prostitutas en busca de clientes, por lo que al acto de ir de fiesta y hacer bromas entre risas pasó a ser conocido como cachondeo.\r\n\r\nPero tal y como indico al inicio de esta curiosidad, hay quien no está de acuerdo con esta explicación señalando que la raíz etimológica del término cachondeo está en la palabra ‘cachorro’. Y es por el ruido y alboroto que ocasionan las crías de perros lo que le dio origen. Esta tesis fue defendida por el experto Joan Corominas en su Breve Diccionario Etimológico de la Lengua Española. Cabe destacar que el Diccionario de la RAE en su entrada ‘cachondo’ señala el mismo origen etimológico aportado por Corominas.', 'Alfredito Cominito', '2018-05-01 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCat` int(11) NOT NULL,
  `nombreCat` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcionCat` longtext COLLATE utf8_spanish2_ci,
  `imagenCat` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCat`, `nombreCat`, `descripcionCat`, `imagenCat`) VALUES
(1, 'Primavera', 'Los artículos mas vistosos y alegres para esta primavera', NULL),
(2, 'Verano', 'Para disfrutar de un fresquito y divertido verano playero', NULL),
(3, 'Otoño', 'Los colores más cálidos y serios para la transición del calor al frío', NULL),
(4, 'Invierno', 'Prepárate para no pasar frío y estar actualizado en tu indumentaria invernal', NULL),
(5, 'Nueva categoria', '<p><b>Probamos </b><u>más </u>cosas para ver si se <font color=\"#00369b\">cambian </font>bien</p>', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `idImg` int(11) NOT NULL,
  `descripcionImg` text COLLATE utf8_spanish2_ci,
  `archivoImg` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `idProd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`idImg`, `descripcionImg`, `archivoImg`, `idProd`) VALUES
(1, NULL, '1526988985_boina1.jpg', 21),
(2, NULL, '1526991168_boina2.jpg', 21),
(3, NULL, '1526987700_boina3.jpg', 21),
(4, NULL, '1526989993_gafas1.jpg', 22),
(5, NULL, '1526988776_gafas2.jpg', 22),
(6, NULL, '1526990371_gafas3.jpg', 22),
(7, NULL, '1526988336_zapato1.jpg', 23),
(8, NULL, '1526996629_zapato2.jpg', 23),
(9, NULL, '1526995985_zapato3.jpg', 23),
(10, NULL, '1527076059_1526991168_boina2.jpg', 1),
(11, NULL, '1527102469_bufanda1.jpg', 2),
(12, NULL, '1527107773_bufanda2.jpg', 2),
(13, NULL, '1527105579_bufanda3.jpg', 2),
(14, NULL, '1527108382_boina4.jpg', 1),
(15, NULL, '1527099754_boina5.jpg', 1),
(16, NULL, '1527105975_guantes1.jpg', 3),
(17, NULL, '1527099081_guantes2.jpg', 3),
(18, NULL, '1527107048_guantes3.jpg', 3),
(19, NULL, '1527101978_orejeras1.jpg', 4),
(20, NULL, '1527106768_orejeras2.jpg', 4),
(21, NULL, '1527107765_orejeras3.jpg', 4),
(22, NULL, '1527104690_botas1.jpg', 5),
(23, NULL, '1527106829_botas2.jpg', 5),
(24, NULL, '1527106603_botas3.jpg', 5),
(25, NULL, '1527107340_anork1.jpg', 6),
(26, NULL, '1527103561_anork2.jpg', 6),
(27, NULL, '1527101004_anork3.jpg', 6),
(28, NULL, '1527106040_camisa1.jpg', 7),
(29, NULL, '1527101375_camisa2.jpg', 7),
(30, NULL, '1527107995_camisa3.jpg', 7),
(31, NULL, '1527106812_guantesb1.jpg', 8),
(32, NULL, '1527102033_guantesb2.jpg', 8),
(33, NULL, '1527105485_guantesb3.jpg', 8),
(34, NULL, '1527108275_gafasc1.jpg', 9),
(35, NULL, '1527108690_gafasc2.jpg', 9),
(36, NULL, '1527100669_gafasc3.jpg', 9),
(37, NULL, '1527109743_pantalonc1.jpg', 10),
(38, NULL, '1527103192_pantalonc2.jpg', 10),
(39, NULL, '1527109439_pantalonc3.jpg', 10),
(40, NULL, '1527108886_chaqueta1.jpg', 11),
(41, NULL, '1527106200_chaqueta2.jpg', 11),
(42, NULL, '1527105136_chaqueta3.jpg', 11),
(43, NULL, '1527103937_traje1.jpg', 12),
(44, NULL, '1527106614_traje2.jpg', 12),
(45, NULL, '1527101457_traje3.jpg', 12),
(46, NULL, '1527110538_sombrero1.jpg', 17),
(47, NULL, '1527105675_sombrero2.jpg', 17),
(48, NULL, '1527108465_sombrero3.jpg', 17),
(49, NULL, '1527107488_sandalias1.jpg', 15),
(50, NULL, '1527101800_sandalias2.jpg', 15),
(51, NULL, '1527109222_sandalias3.jpg', 15),
(52, NULL, '1527163512_zapa1.jpg', 14),
(53, NULL, '1527160278_zapa2.jpg', 14),
(54, NULL, '1527157314_zapa3.jpg', 14),
(55, NULL, '1527165931_moca1.jpg', 16),
(56, NULL, '1527166020_moca2.jpg', 16),
(57, NULL, '1527161235_moca3.jpg', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `idPago` int(11) NOT NULL,
  `nombrePago` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `idPedido` int(11) NOT NULL,
  `fechaPedido` date NOT NULL,
  `cantidadPedido` int(11) NOT NULL,
  `idProd` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idPago` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProd` int(11) NOT NULL,
  `nombreProd` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcionProd` text COLLATE utf8_spanish2_ci,
  `precioProd` double(5,2) NOT NULL,
  `unidadesProd` int(11) NOT NULL,
  `fechaAlta` date NOT NULL,
  `activado` bit(1) NOT NULL,
  `idCat` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProd`, `nombreProd`, `descripcionProd`, `precioProd`, `unidadesProd`, `fechaAlta`, `activado`, `idCat`) VALUES
(1, 'boina', 'boina muy bonita de lana', 25.00, 10, '2018-05-15', b'1', 4),
(2, 'bufanda', 'Bufanda de inverno lana pura a colorines', 15.00, 5, '2018-05-03', b'1', 4),
(3, 'guantes', 'Guantes de  piel muy abrigados para crudos inviernos', 50.00, 50, '2018-04-11', b'1', 4),
(4, 'orejeras', 'Producto de importación muy exitoso', 15.00, 150, '2018-05-08', b'1', 4),
(5, 'botas de agua', 'Para días lluviosos, boticas con forro de piel, muy calenticas e impermeables', 65.00, 25, '2018-05-03', b'1', 3),
(6, 'anorak', 'Doble capa con relleno de plumas', 125.00, 50, '2018-05-03', b'1', 4),
(7, 'camisa', ' termica muy calentica', 10.00, 100, '2018-05-08', b'1', 3),
(8, 'guantes bluetooth', 'con bluetooth todo es mejor', 25.00, 200, '2018-05-08', b'1', 2),
(9, 'gafas de sol', 'En todos los colores y para todo tipo de soles', 50.00, 25, '2018-05-15', b'1', 1),
(10, 'pantalón corto', 'Aventura y paseo multitarea y multitejido', 30.00, 100, '2018-05-15', b'1', 1),
(11, 'chaqueta americana', 'Producto de lino para un entretiempo entreverado', 125.00, 10, '2018-05-15', b'1', 2),
(12, 'traje azul', 'Bonito conjunto de americana y pantalón de hilo fino', 225.00, 15, '2018-05-15', b'1', 2),
(13, 'camisa de cuadros gris', 'Cuadros para todos los gustos de tamaños y formas distintas', 15.00, 50, '2018-05-17', b'0', 2),
(14, 'zapatillas paseo', 'Zapatillas de tela de toda la vida transpirables', 30.00, 25, '2018-05-17', b'1', 3),
(15, 'sandalias romanas', 'de badana fina y suela resistente de caucho', 50.00, 10, '2018-05-18', b'1', 2),
(16, 'mocasines chica', 'típicos de india de toda la vida', 15.00, 150, '2018-05-16', b'1', 1),
(17, 'sombrero de paja', 'Para todo el mundo proteje del sol y refresca el ambiente', 25.00, 25, '2018-05-09', b'1', 2),
(18, 'toalla playera a raya', 'Grande y muy suave', 100.00, 5, '2018-05-09', b'0', 2),
(19, 'chaqueta de punto rosa', 'Para chicas fresquitas fina y elegante', 40.00, 10, '2018-05-09', b'0', 1),
(20, 'bufanda a cuadros', 'Franela de alta calidad', 75.00, 10, '2018-05-15', b'1', 4),
(21, 'Otra boina', 'variedad de boinas de todas las formas', 50.00, 25, '2018-05-22', b'1', 4),
(22, 'Gafas de sol modernas', 'Gafas muy modernas y aptas para todos los públicos', 75.00, 50, '2018-05-22', b'1', 2),
(23, 'Zapato ', 'Gran variedad de zapatos para todos los usos', 125.00, 50, '2018-05-22', b'1', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `idProveedor` int(11) NOT NULL,
  `nombreProveedor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `telefonoProveedor` varchar(9) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`idProveedor`, `nombreProveedor`, `telefonoProveedor`) VALUES
(1, 'José Proveedor Uno', '976000001'),
(2, 'Pepe Proveedor Dos', '976000002'),
(3, 'Pepe Luis Proveedor Tres', '976000003');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `login` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `session` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `avatar` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombre`, `apellidos`, `login`, `password`, `correo`, `session`, `avatar`) VALUES
(1, 'Jose Ignacio', 'Lahoz Felez', 'admin', 'ee10c315eba2c75b403ea99136f5b48d', 'admin@gmail.com', 'c755552f5bb589632331d8efc35d5d32', NULL),
(2, 'Pepe Lui', 'Leches Frescas', 'Alparcero', 'ee10c315eba2c75b403ea99136f5b48d', 'pepe@gmail.com', '', NULL),
(3, 'Pepito', 'Listillo caprichoso', 'Listin', '81dc9bdb52d04dc20036dbd8313ed055', 'pepito@gmail.com', '', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCat`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`idImg`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`idPago`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`idPedido`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProd`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `idImg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `idPago` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
