<?php 
//Fichero models/blogModel.php

class Blog{
	public $entradas; //Será un vector de posts

	public function __construct(){
		$this->entradas=[]; // Le indico que será un vector vacio de momento
	}

	public function dimeEntradas(){
		global $conexion; //Hago alusión a la conexión global
		$sql="SELECT * FROM blog ORDER BY fecha DESC";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->entradas[]=new Post($registro);
		}
		return $this->entradas; //Devuelve un array de posts

	}

	public function dimeEntrada($id){
		global $conexion; //Hago alusión a la conexión global
		$sql="SELECT * FROM blog WHERE id=$id";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		$entrada=new Post($registro);
		return $entrada; //Devuelve un solo post

	}

} //Fin de la class Blog

?>